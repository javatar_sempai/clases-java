CREATE TABLE `veterinaria`.`raza` (
  `id` VARCHAR(100) NOT NULL,
  `nombre` VARCHAR(45) NULL,
  `id_especie` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_raza_1_idx` (`id_especie` ASC),
  CONSTRAINT `fk_raza_1`
    FOREIGN KEY (`id_especie`)
    REFERENCES `veterinaria`.`especie` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
