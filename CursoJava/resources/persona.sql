CREATE TABLE `veterinaria`.`persona` (
  `id` VARCHAR(100) NOT NULL,
  `nombre` VARCHAR(45) NULL,
  `ape_paterno` VARCHAR(45) NULL,
  `ape_materno` VARCHAR(45) NULL,
  `dni` VARCHAR(45) NULL,
  `direccion` VARCHAR(45) NULL,
  `celular` VARCHAR(45) NULL,
  `fecha_nacimiento` DATETIME NULL,
  PRIMARY KEY (`id`));
