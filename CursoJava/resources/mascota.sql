CREATE TABLE `veterinaria`.`mascota` (
  `id` VARCHAR(100) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `sexo` VARCHAR(45) NULL,
  `nro_ficha` VARCHAR(45) NULL,
  `nro_microchip` VARCHAR(45) NULL,
  `fecha_nacimiento` DATETIME NULL,
  `color` VARCHAR(45) NULL,
  `id_propietario` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

  
  ALTER TABLE `veterinaria`.`mascota` 
ADD INDEX `fk_mascota_1_idx` (`id_propietario` ASC);
ALTER TABLE `veterinaria`.`mascota` 
ADD CONSTRAINT `fk_mascota_1`
  FOREIGN KEY (`id_propietario`)
  REFERENCES `veterinaria`.`persona` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
  ALTER TABLE `veterinaria`.`mascota` 
ADD COLUMN `id_raza` VARCHAR(100) NULL AFTER `id_propietario`,
ADD INDEX `fk_mascota_2_idx` (`id_raza` ASC);
ALTER TABLE `veterinaria`.`mascota` 
ADD CONSTRAINT `fk_mascota_2`
  FOREIGN KEY (`id_raza`)
  REFERENCES `veterinaria`.`raza` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
