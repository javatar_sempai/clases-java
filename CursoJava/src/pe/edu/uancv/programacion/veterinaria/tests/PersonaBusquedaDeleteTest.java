package pe.edu.uancv.programacion.veterinaria.tests;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pe.edu.uancv.programacion.veterinaria.model.Persona;

public class PersonaBusquedaDeleteTest {

	public static void main(String[] args) {
		List<Persona> lista = new ArrayList<>();

		Persona p1 = new Persona();
		p1.setId("1");
		p1.setNombre("Jesus");
		p1.setDni("123123123");

		Persona p2 = new Persona();
		p1.setId("2");
		p2.setNombre("Vladimir");
		p2.setDni("123123123");

		Persona p3 = new Persona();
		p1.setId("3");
		p3.setNombre("Sandra");
		p3.setDni("123123123");

		lista.add(p1);
		lista.add(p2);
		lista.add(p3);

		for (Persona p : lista) {
			System.out.println(p.getNombre() + " - " + p.getDni());
		}

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingresar termino de busqueda");

		String q = scan.next();

		for (int i = 0; i < lista.size(); i++) {

			if (lista.get(i).getNombre().contains(q)) {
				lista.remove(i);
			}
		}

		for (Persona p : lista) {
			System.out.println(p.getNombre() + " - " + p.getDni());
		}
	}

}
