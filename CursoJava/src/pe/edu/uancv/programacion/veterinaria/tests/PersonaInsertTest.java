package pe.edu.uancv.programacion.veterinaria.tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import pe.edu.uancv.programacion.veterinaria.model.Persona;

public class PersonaInsertTest {

	public static void main(String[] args) {

		List<Persona> personas = new ArrayList<>();
		
		Scanner scan = new Scanner(System.in);

		for (int i = 0; i < 5; i++) {
			Persona p = new Persona();
			Date fid = new Date();
			p.setId(fid.getTime() + "");

			System.out.println("----- Registro de Persona -----");

			System.out.println("Ingrese el nombre:");
			p.setNombre(scan.next());

			System.out.println("Ingrese el apellido paterno:");
			p.setApePaterno(scan.next());

			System.out.println("Ingrese el apellido materno:");
			p.setApeMaterno(scan.next());

			System.out.println("Ingrese el DNI:");
			p.setDni(scan.next());

			System.out.println("Ingrese la direccion:");
			p.setDireccion(scan.next());

			System.out.println("Ingrese el nro de celular:");
			p.setCelular(scan.next());
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			System.out.println("Ingrese la fecha de nacimiento:");
			try {
				p.setFechaNacimiento(sdf.parse(scan.next()));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			System.out.println("----- Fin de registro -----");
			personas.add(p);
		}

		for (Persona per : personas) {
			System.out.println("Nombre: " + per.getNombre() + "\tDNI:\t"
					+ per.getDni());
		}

	}

}
