package pe.edu.uancv.programacion.veterinaria.tests;

import java.util.ArrayList;
import java.util.List;

import pe.edu.uancv.programacion.veterinaria.model.Persona;

public class PersonaUpdateTest {

	public static void main(String[] args) {
		List<Persona> lista = new ArrayList<>();

		Persona p1 = new Persona();
		p1.setId("1");
		p1.setNombre("Jesus");
		p1.setDni("123123123");

		Persona p2 = new Persona();
		p1.setId("2");
		p2.setNombre("Vladimir");
		p2.setDni("123123123");

		Persona p3 = new Persona();
		p1.setId("3");
		p3.setNombre("Sandra");
		p3.setDni("123123123");

		lista.add(p1);
		lista.add(p2);
		lista.add(p3);

		for (Persona p : lista) {
			System.out.println(p.getNombre() + " - " + p.getDni());
		}

		System.out.println();
		System.out.println("------------------------------");
		System.out.println();

		lista.get(1).setNombre("Edwin " + lista.get(1).getNombre());

		for (Persona p : lista) {
			System.out.println(p.getNombre() + " - " + p.getDni());
		}

	}

}
