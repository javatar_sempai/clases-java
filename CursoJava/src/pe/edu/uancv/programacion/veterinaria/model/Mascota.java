package pe.edu.uancv.programacion.veterinaria.model;

import java.util.Date;

public class Mascota {

	private String id;
	private String nombre;
	private String sexo;
	private String nroFicha;
	private String nroMicrochip;
	private Date fechaNacimiento;
	private String color;
	private Raza raza;
	private Persona propietario;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Persona getPropietario() {
		return propietario;
	}

	public void setPropietario(Persona propietario) {
		this.propietario = propietario;
	}

	public String getNroFicha() {
		return nroFicha;
	}

	public void setNroFicha(String nroFicha) {
		this.nroFicha = nroFicha;
	}

	public String getNroMicrochip() {
		return nroMicrochip;
	}

	public void setNroMicrochip(String nroMicrochip) {
		this.nroMicrochip = nroMicrochip;
	}

	public Raza getRaza() {
		return raza;
	}

	public void setRaza(Raza raza) {
		this.raza = raza;
	}

}
