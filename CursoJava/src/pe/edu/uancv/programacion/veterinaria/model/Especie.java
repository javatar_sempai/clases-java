package pe.edu.uancv.programacion.veterinaria.model;

import java.util.List;

public class Especie {

	private String id;
	private String nombre;
	private List<Raza> razas;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Raza> getRazas() {
		return razas;
	}

	public void setRazas(List<Raza> razas) {
		this.razas = razas;
	}

}
