package pe.edu.uancv.programacion.veterinaria.model;

public class Tratamiento {

	private String id;
	private String detalle;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

}
