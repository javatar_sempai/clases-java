package pe.edu.uancv.programacion.veterinaria.model;

import java.util.Date;
import java.util.List;

public class HistoriaClinica {

	private String id;
	private String nro;
	private String ubicacion;
	private Mascota mascota;
	private Date fechaRegistro;
	private List<HistoriaDetalle> detalles;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNro() {
		return nro;
	}

	public void setNro(String nro) {
		this.nro = nro;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Mascota getMascota() {
		return mascota;
	}

	public void setMascota(Mascota mascota) {
		this.mascota = mascota;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public List<HistoriaDetalle> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<HistoriaDetalle> detalles) {
		this.detalles = detalles;
	}

}
