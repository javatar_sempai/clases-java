package pe.edu.uancv.programacion.veterinaria.model;

import java.util.Date;

public class HistoriaDetalle {

	private String id;
	private HistoriaClinica historia;
	private TipoAtencion tipoAtencion;
	private String diagnostico;
	private Date fechaConsulta;
	private Date fechaConsultaProgramada;
	private Tratamiento tratamiento;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public HistoriaClinica getHistoria() {
		return historia;
	}

	public void setHistoria(HistoriaClinica historia) {
		this.historia = historia;
	}

	public TipoAtencion getTipoAtencion() {
		return tipoAtencion;
	}

	public void setTipoAtencion(TipoAtencion tipoAtencion) {
		this.tipoAtencion = tipoAtencion;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public Date getFechaConsulta() {
		return fechaConsulta;
	}

	public void setFechaConsulta(Date fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	public Date getFechaConsultaProgramada() {
		return fechaConsultaProgramada;
	}

	public void setFechaConsultaProgramada(Date fechaConsultaProgramada) {
		this.fechaConsultaProgramada = fechaConsultaProgramada;
	}

	public Tratamiento getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(Tratamiento tratamiento) {
		this.tratamiento = tratamiento;
	}

}
