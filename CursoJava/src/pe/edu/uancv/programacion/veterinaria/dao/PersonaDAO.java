package pe.edu.uancv.programacion.veterinaria.dao;

import java.util.List;

import pe.edu.uancv.programacion.veterinaria.model.Persona;

public interface PersonaDAO {

	void insertar(Persona p);

	void actualizar(Persona p);

	void eliminar(String id);

	List<Persona> listar();

	Persona getById(String id);

}
