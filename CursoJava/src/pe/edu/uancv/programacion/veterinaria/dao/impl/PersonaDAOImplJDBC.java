package pe.edu.uancv.programacion.veterinaria.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.edu.uancv.programacion.veterinaria.connection.DBConn;
import pe.edu.uancv.programacion.veterinaria.dao.PersonaDAO;
import pe.edu.uancv.programacion.veterinaria.model.Persona;

public class PersonaDAOImplJDBC implements PersonaDAO {

	@Override
	public void insertar(Persona p) {
		Connection con = DBConn.getConnection();

		String query = "insert into persona (id,nombre,ape_paterno,ape_materno,"
				+ "dni,direccion,celular,fecha_nacimiento) "
				+ "VALUES(?,?,?,?,?,?,?,?)";

		PreparedStatement ps1;
		try {
			ps1 = con.prepareStatement(query);
			Date d = new Date();
			ps1.setString(1, d.getTime() + "");
			ps1.setString(2, p.getNombre());
			ps1.setString(3, p.getApePaterno());
			ps1.setString(4, p.getApeMaterno());
			ps1.setString(5, p.getDni());
			ps1.setString(6, p.getDireccion());
			ps1.setString(7, p.getCelular());
			ps1.setDate(8, new java.sql.Date(p.getFechaNacimiento().getTime()));

			ps1.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actualizar(Persona p) {
		Connection con = DBConn.getConnection();
		String query = "update persona set nombre=?, ape_paterno=?, ape_materno=?, "
				+ "dni=?, direccion=? celular=?,fecha_nacimiento=? "
				+ "where id=? ";

		try {
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, p.getNombre());
			ps.setString(2, p.getApePaterno());
			ps.setString(3, p.getApeMaterno());
			ps.setString(4, p.getDni());
			ps.setString(5, p.getDireccion());
			ps.setString(6, p.getCelular());
			ps.setDate(7, new java.sql.Date(p.getFechaNacimiento().getTime()));
			ps.setString(8, p.getId());

			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void eliminar(String id) {
		Connection con = DBConn.getConnection();
		String query = "delete from persona where id = ?";
		PreparedStatement ps0;
		try {
			ps0 = con.prepareStatement(query);
			ps0.setString(1, id);
			ps0.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<Persona> listar() {
		List<Persona> lista = new ArrayList<Persona>();

		Connection con = DBConn.getConnection();
		try {

			String query = "select * from persona";

			PreparedStatement ps = con.prepareStatement(query);
			ResultSet rset = ps.executeQuery();

			while (rset.next()) {
				Persona p = new Persona();

				p.setId(rset.getString("id"));
				p.setNombre(rset.getString("nombre"));
				p.setApePaterno(rset.getString("ape_paterno"));
				p.setApeMaterno(rset.getString("ape_materno"));
				p.setDni(rset.getString("dni"));
				p.setDireccion(rset.getString("direccion"));
				p.setCelular(rset.getString("celular"));
				p.setFechaNacimiento(rset.getDate("fecha_nacimiento"));

				lista.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

	@Override
	public Persona getById(String id) {
		Persona p = new Persona();

		Connection con = DBConn.getConnection();

		String query = "select * from persona where id=?";
		try {
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, id);

			ResultSet rset = ps.executeQuery();

			while (rset.next()) {
				p.setId(rset.getString("id"));
				p.setNombre(rset.getString("nombre"));
				p.setApePaterno(rset.getString("ape_paterno"));
				p.setApeMaterno(rset.getString("ape_materno"));
				p.setDni(rset.getString("dni"));
				p.setDireccion(rset.getString("direccion"));
				p.setCelular(rset.getString("celular"));
				p.setFechaNacimiento(rset.getDate("fecha_nacimiento"));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return p;
	}
}
