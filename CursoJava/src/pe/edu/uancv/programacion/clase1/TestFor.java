package pe.edu.uancv.programacion.clase1;

import java.util.Scanner;

public class TestFor {

	public static void main(String[] args) {

		String[] lista = { "Jesus", "Vladimir", "Sandra", "Shazam", "Goku",
				"Yesica" };

		for (int i = 0; i < lista.length; i++) {
			lista[i] = lista[i].toUpperCase();
		}

		for (String nombre : lista) {
			System.out.println(nombre);
		}

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese termino de busqueda:");
		String q = scan.next();

		// Recorre cada item del arreglo
		for (String nombre : lista) {
			// Compara si el valor del item contiene el termino de busqueda
			if (nombre.contains(q.toUpperCase())) {
				System.out.println(nombre);
			}
		}

	}
/*	Ejercicio 01
 * Elecciones: El programa debe retornar al ganador de las elecciones, 
 * 			junto con las cantidades de votos de cada candidato;
 * 			los candidatos son: Goku, Vegeta, Gohan
 * 		- Poblacion : 10
 * 
 * 
*/
}
