package pe.edu.uancv.programacion.clase1;

import java.util.Scanner;

public class TestWhile {

	public static void main(String[] args) {

		Operaciones v = new Operaciones();

		String PF = "";

		Scanner scan = new Scanner(System.in);

		do {
			System.out.println("Ingrese nota 1:");
			double n1 = scan.nextDouble();
			System.out.println("Ingrese nota 2:");
			double n2 = scan.nextDouble();
			System.out.println("Ingrese nota 3:");
			double n3 = scan.nextDouble();

			double promedio = v.promediar(n1, n2, n3);
			PF = v.getPF(promedio);
			System.out.println(PF);

		} while ("DESAPROBADO".equals(PF));

	}
}
