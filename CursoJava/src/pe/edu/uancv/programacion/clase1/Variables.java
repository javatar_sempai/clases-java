package pe.edu.uancv.programacion.clase1;

import java.util.Scanner;

public class Variables {

	public static void main(String[] args) {

		// Persona p = new Persona();
		// p.setNombre("Vladimir");
		// p.setApellidoPaterno("Panca");
		// p.setApellidoMaterno("Mendoza");
		//
		// System.out.println("Edad>>> " + p.getEdad());

		Operaciones v = new Operaciones();

		// System.out.println(v.sumar(2, 3));

		// double promedio = v.promediar(20, 20, 20);
		// System.out.println("Promedio: " + promedio);
		// System.out.println(v.getPF(promedio));

		// int cont = 0;
		// while (cont < 100) {
		// System.out.println("Contador: " + cont);
		// cont++;
		// }
		// System.out.println("Valor Final del Contador: " + cont);
		//
		// cont = 0;
		// do {
		// cont++;
		// System.out.println("Contador: " + cont);
		// } while (cont < 100);
		//
		// System.out.println("Valor Final del Contador: " + cont);
		//
		String PF = "";

		Scanner scan = new Scanner(System.in);

		do {
			System.out.println("Ingrese nota 1:");
			double n1 = scan.nextDouble();
			System.out.println("Ingrese nota 2:");
			double n2 = scan.nextDouble();
			System.out.println("Ingrese nota 3:");
			double n3 = scan.nextDouble();

			double promedio = v.promediar(n1, n2, n3);
			PF = v.getPF(promedio);
			System.out.println(PF);

		} while ("DESAPROBADO".equals(PF));

	}
}
