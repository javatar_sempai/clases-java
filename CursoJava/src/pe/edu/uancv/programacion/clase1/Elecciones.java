package pe.edu.uancv.programacion.clase1;

import java.util.Scanner;

public class Elecciones {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		int goku = 0;
		int vegeta = 0;
		int gohan = 0;

		for (int i = 0; i < 10; i++) {
			System.out.println("Bienvenido a las Elecciones 2015");
			System.out.println("Ingrese el número de su candidato");
			System.out.println("1 - Goku");
			System.out.println("2 - Vegeta");
			System.out.println("3 - Gohan");

			String voto = scan.next();
			if ("1".equals(voto)) {
				goku++;
			}
			if ("2".equals(voto)) {
				vegeta++;
			}
			if ("3".equals(voto)) {
				gohan++;
			}
			System.out.println("---------------------------------------");
		}

		if (goku > vegeta && goku > gohan) {
			System.out.println("Ganador Goku, con " + goku + " votos.");
		} else if (vegeta > gohan) {
			System.out.println("Ganador Vegeta, con " + vegeta + " votos.");
		} else {
			System.out.println("Ganador Gohan, con " + gohan + " votos.");
		}

	}

}
