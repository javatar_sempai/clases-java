package pe.edu.uancv.programacion.clase1;

public class Operaciones {

	public int sumar(int a, int b) {
		int c = a + b;
		return c;
	}

	public double promediar(double nota1, double nota2, double nota3) {
		double promedio = (nota1 + nota2 + nota3) / 3;
		return promedio;
	}

	public String getPF(double promedio) {

		if (promedio == 20) {
			return "SUPERAPROBADO";
		} else if (promedio >= 11 && promedio < 20) {
			return "APROBADO";
		} else {
			return "DESAPROBADO";
		}
	}

}
