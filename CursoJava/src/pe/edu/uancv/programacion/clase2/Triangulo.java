package pe.edu.uancv.programacion.clase2;

public class Triangulo extends Poligono {

	public Triangulo(int base, int altura) {
		super.setBase(base);
		super.setAltura(altura);
	}

	public Triangulo() {
	}

	@Override
	int calcularArea() {
		return (super.getBase() * super.getAltura()) / 2;
	}

	public String saludar() {
		return "Hola";
	}

	public String saludar(String nombre) {
		return "Hola " + nombre;
	}

	public String saludar(String nombre1, String nombre2) {
		return "Hola " + nombre1 + " y Hola " + nombre2;
	}

}
