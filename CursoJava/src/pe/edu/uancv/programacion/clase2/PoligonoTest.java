package pe.edu.uancv.programacion.clase2;

public class PoligonoTest {

	public static void main(String[] args) {

		Triangulo t = new Triangulo(10, 15);

		t.setBase(10);
		t.setAltura(10);

		System.out.println("Area del triangulo: " + t.calcularArea());

		Cuadrilatero c = new Cuadrilatero();
		c.setBase(12);
		c.setAltura(13);

		System.out.println("Area del cuadrilatero: " + c.calcularArea());

		System.out.println(t.saludar());
		System.out.println(t.saludar("Jesus"));
		System.out.println(t.saludar("Sandra", "Vladimir"));

	}

}
