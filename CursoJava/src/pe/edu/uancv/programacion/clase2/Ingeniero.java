package pe.edu.uancv.programacion.clase2;

public class Ingeniero extends Persona {

	private String cip;
	private String especialidad;
	private String nroTitulo;

	public String getCip() {
		return cip;
	}

	public void setCip(String cip) {
		this.cip = cip;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public String getNroTitulo() {
		return nroTitulo;
	}

	public void setNroTitulo(String nroTitulo) {
		this.nroTitulo = nroTitulo;
	}

}
