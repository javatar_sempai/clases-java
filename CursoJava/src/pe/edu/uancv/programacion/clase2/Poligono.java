package pe.edu.uancv.programacion.clase2;

public abstract class Poligono {

	private int lados;

	abstract int calcularArea();

	private int base;
	private int altura;

	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public int getLados() {
		return lados;
	}

	public void setLados(int lados) {
		this.lados = lados;
	}

}
