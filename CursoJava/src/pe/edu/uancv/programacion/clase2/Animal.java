package pe.edu.uancv.programacion.clase2;

public abstract class Animal {

	private String nombre;
	private int edad;
	private int peso;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	public String nacer() {
		return "Nacer generico";
	}

	public String caminar() {
		return "Caminar Generico";
	}

	public String morir() {
		return "Morir generico";
	}

}
